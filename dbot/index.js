


var fs=require('fs');
var data=fs.readFileSync('restaurants.json', 'utf8');
const words=JSON.parse(data);
var bodyparser=require('body-parser');
const express = require('express')
const diff = require("dialogflow-fulfillment")
const { platform } = require('os');
const app = express()

app.get('/',(req,res)=>{
    res.send('operativni smo')
})

app.post('/',express.json(),(req,res)=>{
    const agent = new diff.WebhookClient({
        request:req,
        response:res
    })

     function demoWebhook(agent){
         agent.add("saljemo poruku sa webhook servera")
     }

     function restaurants(food){
      let restaurants = words.filter(el => el.meals.find(me => me.category == food) !=null).map(elem=> elem.name);

      return restaurants;
     }

     function returnAverage(arr){
       let sum = 0;
       for (let index = 0; index < arr.length; index++) {
        sum+=arr[index]
         
       }

      return sum/arr.length
     }

    function return5BestRestaurants(){
     let topRes = words.map((el)=>{
        return {
          restaurant: el.name,
          avgMark:returnAverage(el.marks)
        }
        })
      
    
   

       return topRes.sort((a,b)=>a.avgMark - b.avgMark).reverse().map(res => res.restaurant).slice(0,4)
    }

     function orderFood(agent){
    
    let category = agent.context.get('awaiting_kinf_of_food').parameters['kind_of_food'];
     let restaurants1 = restaurants(category)
      console.log("------------------------")
     console.log(return5BestRestaurants())
     console.log("------------------------")
 

   console.log(restaurants1)
     
       agent.add("Ovo su restoranu iz kojih mozete naruciti  " +category)
       
     for(res of restaurants1 ){
       agent.add(res)
     }
     

     }

     function  returnRecomendation(){
       agent.add("Ovos su nasih to 5 restorana,sigurno ce se naci nesto za vas")
        let restaurants = return5BestRestaurants();
        console.log(restaurants)

        for(item of restaurants){
         agent.add(item)
       } 

      
     }


     function customintent(agent){
       let mile = "mile"
        const payload = {
          "richContent": [
            [
              {
                "type": "chips",
                "options": [
                  {
                    "text": "Chip 1",
                 
                  },
                  {
                    "text": "Chip 2",
                 
                  }
                ]
              }
            ]
          ]
          }
          agent.add(new diff.Payload(agent.UNSPECIFIED ,payload,{sendAsMessage:true,rawPayload:true}))
     }

    

    var intentmap = new Map()

    intentmap.set('webendpoint',demoWebhook)
    intentmap.set('customintent',customintent)
    intentmap.set('0111_vrsta_hrane',orderFood)
    intentmap.set('012_inicijalni_upit_odgovor_odrican',returnRecomendation)

    agent.handleRequest(intentmap)
})

app.listen(3500,()=>{console.log("server radi na portu 3500")})